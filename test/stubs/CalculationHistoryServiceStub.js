import { HistoryStub } from './HistoryStub';

export class CalculationHistoryServiceStub {
    constructor(uncalculatedFees) {
        this.historyStub = new HistoryStub(uncalculatedFees);
    }

    retrieveHistory(service) {
        return this.historyStub;
    }

    verifyAppliedSum(expectedSum){
        this.historyStub.verifyAppliedSum(expectedSum);
    }
}