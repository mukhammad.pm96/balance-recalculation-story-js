---
focus: src/UserAccount.js:1
---
### Finish

Once you finish refactoring, it’s important to re-read the code and make sure it is clean.

In this coding story, you discovered that even complex functions with a large number of arguments can be refactored in a simple way, even without creating new classes. Unit tests and the refactoring capabilities of the IDE are a great help. You always can play around with the code, trying different kinds of refactoring in different places.

Keep your code clean and readable. 


<quiz>
  <question>What is the right way to reduce the number of arguments?</question>
  <answer>Analyze how each of the arguments are used</answer>
  <answer>Play with the code using refactoring tools and unit tests</answer>
  <answer>Extract classes</answer>
  <answer correct>All of the above</answer>
</quiz>
